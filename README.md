# Convert to NSV task file

This is a simple script that converts coordinate datasets to the NSV Task File format.

Right now, it's specifically written to convert the TwinLife dataset to this format. But I might expand on it later.

## NSV Task File

A file in the "NSV task file format" is simply a text file with this structure:

```
id1,lat,lng
id2,lat,lng
...
```

It does also support some other modes, but each of them is this basic structure: A unique ID, and then coordinates in WGS84. All of my tools from the [ENPGT Toolbox](https://git.mpib-berlin.mpg.de/metzner/enpgt-toolbox) are written to support this exact input format, and I do not want to rewrite them to support other formats, so I'd rather write a conversion script like this.
