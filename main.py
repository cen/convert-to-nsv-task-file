import sys
from pathlib import Path

from pandas import read_csv
from pyproj import Proj, Transformer

PROJ_WGS_84 = Proj("EPSG:4326")


def convert_to_nsv(input_path: Path, output_path: Path = Path("task.txt"), proj: Proj = Proj("EPSG:25832")):
    assert input_path != output_path, "Input file path and output file path are the same."

    df = read_csv(input_path)

    transproj = Transformer.from_proj(proj, PROJ_WGS_84, always_xy=True)

    with open(output_path, "w") as output_file:
        for geo_id, xx, yy in zip(df.geo_id, df.X, df.Y):
            lng, lat = transproj.transform(xx, yy)

            output_file.write(f"{geo_id},{lat},{lng}\n")


def main(args):
    task_file_name = "TwinLife_testdata_v1.0.0.csv"

    for arg in args:
        if Path(arg).is_file():
            task_file_name = arg
            continue

        raise ValueError(f"Don't understand argument {arg}")

    convert_to_nsv(Path(task_file_name))


if __name__ == "__main__":
    main(sys.argv[1:])
